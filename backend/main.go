package main

import (
	"context"
	"errors"
	"github.com/genjidb/genji"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/SGT911/passwd/backend/db"
	"gitlab.com/SGT911/passwd/backend/routes"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

var (
	Logger *log.Logger
	DB     *genji.DB
	Secret string
)

func main() {
	Logger = log.New(os.Stdout, "[server] ", log.LstdFlags)

	expose := os.Getenv("EXPOSE")
	transport := "tcp"
	if expose == "" {
		expose = ":9090"
	} else if strings.HasPrefix(expose, "/") || strings.HasPrefix(expose, "./") {
		transport = "unix"
	}

	l, err := net.Listen(transport, expose)
	if err != nil {
		log.Fatal(err)
	}

	dbPath := os.Getenv("DBPATH")
	if dbPath == "" {
		dbPath = "./db.genji"
	}

	Logger.Println("Opening database")
	DB, _ = genji.Open(dbPath)
	if err := db.InitializeDB(DB); err != nil {
		Logger.Fatal(err)
	}

	Secret = os.Getenv("SECRET")
	if Secret == "" {
		Logger.Println("WARNING: Using default secret ('a1b2c3d4'), please consider change it with environment $SECRET")
		Secret = "a1b2c3d4"
	}

	r := chi.NewRouter()
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			if res.Header().Get("Content-Type") == "" {
				res.Header().Set("Content-Type", "application/json; charset=utf-8")
			}
			next.ServeHTTP(res, req)
		})
	})
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			if req.Header.Get("Origin") == "" {
				req.Header.Set("Origin", "*")
			}
			next.ServeHTTP(res, req)
		})
	}, cors.New(cors.Options{
		AllowOriginFunc: func(r *http.Request, origin string) bool {
			return true //  TODO: Check with an EnvVar
		},
		MaxAge:           300,
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "POST", "DELETE", "PUT", "OPTIONS"},
		ExposedHeaders:   []string{"Content-Type", "Content-Length", "Set-JWT-Token"},
		AllowedHeaders:   []string{"Content-Type", "Content-Length", "JWT-Token", "Authorization"},
	}).Handler, func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			if req.Method == "OPTIONS" {
				res.WriteHeader(204)
				return
			} else {
				next.ServeHTTP(res, req)
			}
		})
	}) // TODO: Create in a struct middleware

	r.Get("/ping", routes.Ping)
	r.Route("/users", (&routes.UserHandler{
		DB:     DB,
		Secret: Secret,
	}).LoadRouter)
	r.Route("/keys", (&routes.KeyHandler{
		DB:     DB,
		Secret: Secret,
	}).LoadRouter)
	r.Route("/store", (&routes.StoreHandler{
		DB:     DB,
		Secret: Secret,
	}).LoadRouter)

	server := &http.Server{
		Handler: r,
	}

	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		Logger.Printf("Listening server on: %#v", expose)
		if err = server.Serve(l); err != nil && !errors.Is(err, http.ErrServerClosed) {
			Logger.Fatal(err)
		}
	}()

	<-exit
	Logger.Println("Stopping server")
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		Logger.Fatal(err)
	}

	if err := DB.Close(); err != nil {
		Logger.Fatal(err)
	}

	Logger.Println("Server was closed")
}
