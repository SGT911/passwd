package db

import (
	"github.com/genjidb/genji"
	"log"
	"os"
)

func InitializeDB(db *genji.DB) error {
	logger := log.New(os.Stdout, "[db] ", log.LstdFlags)

	logger.Println("Creating 'user' table")
	if err := db.Exec(
		"CREATE TABLE IF NOT EXISTS user (" +
			"email TEXT NOT NULL," +
			"nick TEXT," +
			"PRIMARY KEY (email)" +
			");",
	); err != nil {
		logger.Println("Error creating 'user' table")
		return err
	}

	logger.Println("Creating 'keys' table")
	if err := db.Exec(
		"CREATE TABLE IF NOT EXISTS keys (" +
			"id TEXT NOT NULL," +
			"owner TEXT NOT NULL," +
			"name TEXT NOT NULL DEFAULT ('Principal')," +
			"priv BLOB NOT NULL," +
			"pub BLOB NOT NULL," +
			"PRIMARY KEY (id)," +
			"UNIQUE (owner, name)" +
			");",
	); err != nil {
		logger.Println("Error creating 'keys' table")
		return err
	}

	logger.Println("Creating 'store' table")
	if err := db.Exec(
		"CREATE TABLE IF NOT EXISTS store (" +
			"id TEXT NOT NULL," +
			"key_id TEXT NOT NULL," +
			"name TEXT NOT NULL," +
			"data BLOB NOT NULL," +
			"PRIMARY KEY (id)," +
			"UNIQUE (key_id, name)" +
			");",
	); err != nil {
		logger.Println("Error creating 'store' table")
		return err
	}

	logger.Println("Creating 'shared_keys' table")
	if err := db.Exec(
		"CREATE TABLE IF NOT EXISTS shared_keys (" +
			"user TEXT NOT NULL," +
			"key_id TEXT NOT NULL," +
			"PRIMARY KEY (user, key_id)" +
			");",
	); err != nil {
		logger.Println("Error creating 'shared_keys' table")
		return err
	}

	return nil
}
