package utils

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"time"
)

// CreateKey returns a key pairs of RSA Keys encrypted with password and headers
func CreateKey(strength int, pwd string, owner string) (priv []byte, pub []byte, err error) {
	pkey, err := rsa.GenerateKey(rand.Reader, strength)
	if err != nil {
		return nil, nil, err
	}

	priv = x509.MarshalPKCS1PrivateKey(pkey)
	pub = x509.MarshalPKCS1PublicKey(&pkey.PublicKey)

	createdAt := time.Now().Format(time.RFC3339Nano)

	priv, err = Encrypt(priv, pwd)
	if err != nil {
		return nil, nil, err
	}

	priv = pem.EncodeToMemory(&pem.Block{
		Type: "PRIVATE KEY",
		Headers: map[string]string{
			"Strength":  fmt.Sprintf("%d", strength),
			"Owner":     owner,
			"CreatedAt": createdAt,
		},
		Bytes: priv,
	})

	pub = pem.EncodeToMemory(&pem.Block{
		Type: "PUBLIC KEY",
		Headers: map[string]string{
			"Issuer":    owner,
			"CreatedAt": createdAt,
		},
		Bytes: pub,
	})

	return
}

// DecryptPrivate return the private key decrypted from password
func DecryptPrivate(priv []byte, pwd string) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode(priv)
	if block.Type != "PRIVATE KEY" {
		return nil, errors.New("PEM type is not valid")
	}

	decryptPriv, err := Decrypt(block.Bytes, pwd)
	if err != nil {
		return nil, err
	}

	return x509.ParsePKCS1PrivateKey(decryptPriv)
}

// LoadPublic key from PEM data
func LoadPublic(pub []byte) (*rsa.PublicKey, error) {
	block, _ := pem.Decode(pub)
	if block.Type != "PUBLIC KEY" {
		return nil, errors.New("PEM type is not valid")
	}

	return x509.ParsePKCS1PublicKey(block.Bytes)
}

func RSAEncrypt(key *rsa.PublicKey, content, contentType, fileName string) ([]byte, error) {
	data, err := rsa.EncryptPKCS1v15(rand.Reader, key, []byte(content))
	if err != nil {
		return nil, err
	}

	return pem.EncodeToMemory(&pem.Block{
		Type: "STORE OBJECT",
		Headers: map[string]string{
			"CreatedAt":   time.Now().Format(time.RFC3339Nano),
			"ContentType": contentType,
			"Length":      fmt.Sprintf("%d", len(content)),
			"FileName":    fileName,
		},
		Bytes: data,
	}), nil
}

func RSADecrypt(key *rsa.PrivateKey, data []byte) ([]byte, map[string]string, error) {
	block, _ := pem.Decode(data)
	if block.Type != "STORE OBJECT" {
		return nil, nil, errors.New("PEM type is not valid")
	}

	data, err := rsa.DecryptPKCS1v15(nil, key, block.Bytes)
	if err != nil {
		return nil, nil, err
	}

	return data, block.Headers, nil
}

func GetStoreHeaders(data []byte) (map[string]string, error) {
	block, _ := pem.Decode(data)
	if block.Type != "STORE OBJECT" {
		return nil, errors.New("PEM type is not valid")
	}

	return block.Headers, nil
}