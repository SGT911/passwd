package utils

type (
	ApiError struct {
		Name        string      `json:"name"`
		Description interface{} `json:"desc,omitempty"`
	}

	ApiResponse struct {
		Ok      bool        `json:"ok"`
		Payload interface{} `json:"body,omitempty"`
		Error   *ApiError   `json:"err,omitempty"`
	}
)

// NewOk returns a new Ok Response
func NewOk(payload interface{}) *ApiResponse {
	return &ApiResponse{
		Ok:      true,
		Payload: payload,
	}
}

// NewError returns a new Response with an error
func NewError(err *ApiError) *ApiResponse {
	return &ApiResponse{
		Ok:    false,
		Error: err,
	}
}
