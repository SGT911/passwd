package utils

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"github.com/go-chi/jwtauth"
	"io"
)

var EmptyBody = errors.New("request body is empty")

// JSON make a HTTP Json response.
func JSON(data interface{}) (body []byte) {
	body, _ = json.MarshalIndent(data, "", "\t")
	buff := bytes.NewBuffer(make([]byte, 0))
	json.HTMLEscape(buff, body)
	body = buff.Bytes()
	return
}

// FromJSON get body from request and binds to an interface
func FromJSON(reader io.ReadCloser, data interface{}) (err error) {
	body, err := io.ReadAll(reader)
	if err != nil {
		return
	}

	if len(body) == 0 {
		return EmptyBody
	}

	err = json.Unmarshal(body, data)
	return
}

// FromJWT get a field from token
func FromJWT(ctx context.Context, key string) interface{} {
	token, _, _ := jwtauth.FromContext(ctx)
	val, ok := token.Get(key)
	if !ok {
		return nil
	}

	return val
}
