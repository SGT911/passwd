package routes

import (
	"encoding/base64"
	"errors"
	"github.com/genjidb/genji"
	"github.com/genjidb/genji/document"
	"github.com/genjidb/genji/types"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth"
	"github.com/google/uuid"
	"gitlab.com/SGT911/passwd/backend/utils"
	"net/http"
	"strings"
	"time"
)

type UserHandler struct {
	DB     *genji.DB
	Secret string

	auth     *jwtauth.JWTAuth
	duration time.Duration
}

// LoadRouter create user routing for API
func (h *UserHandler) LoadRouter(r chi.Router) {
	h.auth = jwtauth.New("HS256", []byte(h.Secret), nil)
	h.duration = 15 * time.Minute

	r.Post("/", h.Register)
	r.Post("/login", h.Login)
	r.Get("/exists/{email}", h.Exists)
}

// Register a user with a principal RSA key (returning email & key ID)
func (h *UserHandler) Register(res http.ResponseWriter, req *http.Request) {
	user := new(struct {
		Email    string `json:"email"`
		Nick     string `json:"user"`
		Password string `json:"passwd"`
		Strength int    `json:"strength,omitempty"`
	})

	if err := utils.FromJSON(req.Body, user); err != nil {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	if user.Email == "" || user.Password == "" {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: RequiredError.Error(),
		})))
		return
	}

	if (0 > user.Strength || user.Strength > 5) || len(user.Password) > 32 {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: NotValid.Error(),
		})))
		return
	}

	if user.Strength == 0 {
		user.Strength = 1024
	} else {
		user.Strength = user.Strength * 1024
	}

	id := uuid.New()
	err := h.DB.Update(func(tx *genji.Tx) error {
		nick := user.Nick
		if nick == "" {
			nick = user.Email
		}

		err := tx.Exec("INSERT INTO user VALUES ?", map[string]interface{}{
			"email": user.Email,
			"nick":  nick,
		})
		if err != nil {
			return err
		}

		priv, pub, err := utils.CreateKey(user.Strength, user.Password, user.Email)
		if err != nil {
			return err
		}

		err = tx.Exec("INSERT INTO keys VALUES ?", map[string]interface{}{
			"id":    id.String(),
			"owner": user.Email,
			"priv":  priv,
			"pub":   pub,
		})
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		res.WriteHeader(500)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	res.WriteHeader(201)
	_, _ = res.Write(utils.JSON(utils.NewOk(map[string]string{
		"user":    user.Email,
		"privKey": id.String(),
	})))
}

// Exists returns if a user exists
func (h *UserHandler) Exists(res http.ResponseWriter, req *http.Request) {
	email := chi.URLParam(req, "email")
	name := ""

	err := h.DB.View(func(tx *genji.Tx) error {
		res, err := tx.Query("SELECT nick FROM user WHERE email = ? LIMIT 1", email)
		if err != nil {
			return err
		}

		ok := false

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		err = res.Iterate(func(d types.Document) error {
			ok = true
			vals := make(map[string]interface{})
			err := document.MapScan(d, &vals)
			if err != nil {
				return err
			}

			name = vals["nick"].(string)
			return nil
		})

		if !ok {
			return NotFound
		}
		return nil
	})

	if err != nil {
		if errors.Is(NotFound, err) {
			res.WriteHeader(404)
		} else {
			res.WriteHeader(500)
		}

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	res.WriteHeader(200)
	_, _ = res.Write(utils.JSON(utils.NewOk(name)))
}

func (h *UserHandler) Login(res http.ResponseWriter, req *http.Request) {
	auth := req.Header.Get("Authorization")

	if auth == "" || !strings.HasPrefix(auth, "Basic ") {
		res.WriteHeader(404)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: NotValid.Error(),
		})))
		return
	}

	var value struct {
		ID         uuid.UUID `genji:"id"`
		PrivateKey []byte    `genji:"priv"`
	}

	auth = strings.Replace(auth, "Basic ", "", 1)
	authRaw, _ := base64.URLEncoding.DecodeString(auth)
	auth = string(authRaw)
	dataAuth := strings.SplitN(auth, ":", 2)

	nick := ""
	err := h.DB.View(func(tx *genji.Tx) error {
		res, err := tx.Query("SELECT nick FROM user WHERE email = ?", dataAuth[0])
		if err != nil {
			return err
		}

		ok := false

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		err = res.Iterate(func(d types.Document) error {
			ok = true
			return document.Scan(d, &nick)
		})
		if err != nil {
			return err
		}
		if !ok {
			return NotFound
		}

		res, err = tx.Query("SELECT id, priv FROM keys WHERE owner = ? AND name = 'Principal'", dataAuth[0])
		if err != nil {
			return err
		}

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		err = res.Iterate(func(d types.Document) error {
			return document.StructScan(d, &value)
		})
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		if errors.Is(err, NotFound) {
			res.WriteHeader(404)
		} else {
			res.WriteHeader(500)
		}

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	_, err = utils.DecryptPrivate(value.PrivateKey, dataAuth[1])
	if err != nil {
		res.WriteHeader(401)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "BadCredentials",
			Description: err.Error(),
		})))
		return
	}

	_, token, _ := h.auth.Encode(map[string]interface{}{
		"iat":  time.Now(),
		"exp":  time.Now().Add(h.duration),
		"iss":  dataAuth[0],
		"name": nick,
	})

	res.Header().Set("Set-JWT-Token", token)
	res.WriteHeader(204)
}
