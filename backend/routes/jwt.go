package routes

import (
	"context"
	"github.com/go-chi/jwtauth"
	"gitlab.com/SGT911/passwd/backend/utils"
	"net/http"
)

// JWTValidate from headers ('JWT-Token')
func JWTValidate(auth *jwtauth.JWTAuth) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			token := req.Header.Get("JWT-Token")
			if token == "" {
				res.WriteHeader(401)
				return
			}

			jwt, err := jwtauth.VerifyToken(auth, token)
			if err != nil {
				res.WriteHeader(401)
				_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
					Name:        "BadCredentials",
					Description: err.Error(),
				})))
				return
			}

			ctx := context.WithValue(req.Context(), jwtauth.TokenCtxKey, jwt)
			next.ServeHTTP(res, req.WithContext(ctx))
		})
	}
}
