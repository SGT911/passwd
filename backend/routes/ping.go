package routes

import (
	"gitlab.com/SGT911/passwd/backend/utils"
	"net/http"
)

// Ping return a PONG
func Ping(res http.ResponseWriter, req *http.Request) {
	res.Header().Add("Content-Type", "application/json")
	_, _ = res.Write(utils.JSON(utils.NewOk("PONG")))
}
