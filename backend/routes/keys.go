package routes

import (
	"errors"
	"github.com/genjidb/genji"
	"github.com/genjidb/genji/document"
	"github.com/genjidb/genji/types"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth"
	"github.com/google/uuid"
	"gitlab.com/SGT911/passwd/backend/utils"
	"net/http"
)

type (
	KeyHandler struct {
		DB     *genji.DB
		Secret string

		auth *jwtauth.JWTAuth
	}

	SKey struct {
		ID    string `genji:"id" json:"id"`
		Name  string `genji:"name" json:"name"`
		Owner string `genji:"owner" json:"owner"`
	}
)

// LoadRouter create key routing for API
func (h *KeyHandler) LoadRouter(r chi.Router) {
	h.auth = jwtauth.New("HS256", []byte(h.Secret), nil)

	r.Group(func(r chi.Router) {
		r.Use(JWTValidate(h.auth))

		r.Get("/", h.List)
		r.Post("/", h.New)
		r.Post("/{id}", h.Test)
		r.Put("/share", h.ShareKey)
	})
}

// List keys of actual user in the JWT token
func (h *KeyHandler) List(res http.ResponseWriter, req *http.Request) {
	user := utils.FromJWT(req.Context(), "iss").(string)
	keys := make([]SKey, 0)
	sharedKeys := make([]SKey, 0)

	err := h.DB.View(func(tx *genji.Tx) error {
		res, err := tx.Query("SELECT id, name, owner FROM keys WHERE owner = ?", user)
		if err != nil {
			return err
		}
		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		err = res.Iterate(func(d types.Document) error {
			key := new(SKey)
			err := document.StructScan(d, key)
			if err != nil {
				return err
			}
			keys = append(keys, *key)
			return nil
		})
		if err != nil {
			return err
		}

		res, err = tx.Query("SELECT key_id AS id FROM shared_keys WHERE user = ?", user)
		if err != nil {
			return err
		}
		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		return res.Iterate(func(d types.Document) error {
			key := new(SKey)
			err := document.StructScan(d, key)
			if err != nil {
				return err
			}

			res, err := tx.Query("SELECT name, owner FROM keys WHERE id = ?", key.ID)
			if err != nil {
				return err
			}
			defer func(res *genji.Result) {
				_ = res.Close()
			}(res)
			err = res.Iterate(func(d1 types.Document) error {
				return document.StructScan(d1, key)
			})
			if err != nil {
				return err
			}

			sharedKeys = append(sharedKeys, *key)
			return nil
		})
	})
	if err != nil {
		res.WriteHeader(500)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	_, _ = res.Write(utils.JSON(utils.NewOk(map[string]interface{}{
		"owned":  keys,
		"shared": sharedKeys,
	})))
}

// New key from JWT Token and password
func (h *KeyHandler) New(res http.ResponseWriter, req *http.Request) {
	user := utils.FromJWT(req.Context(), "iss").(string)

	body := new(struct {
		Name     string `json:"name,omitempty"`
		Strength int    `json:"strength,omitempty"`
		Password string `json:"passwd"`
	})

	if err := utils.FromJSON(req.Body, body); err != nil {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	if body.Password == "" {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: RequiredError.Error(),
		})))
		return
	}

	if (len(body.Name) != 0 && len(body.Name) < 4) ||
		(0 > body.Strength || body.Strength > 5) ||
		len(body.Password) > 32 {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: NotValid.Error(),
		})))
		return
	}

	if body.Strength == 0 {
		body.Strength = 1024
	} else {
		body.Strength = body.Strength * 1024
	}

	id := uuid.New()
	err := h.DB.Update(func(tx *genji.Tx) error {
		priv, pub, err := utils.CreateKey(body.Strength, body.Password, user)
		if err != nil {
			return err
		}

		data := map[string]interface{}{
			"id":    id.String(),
			"owner": user,
			"priv":  priv,
			"pub":   pub,
			"name":  body.Name,
		}

		if body.Name == "" {
			data["name"] = id.String()
		}

		err = tx.Exec("INSERT INTO keys VALUES ?", data)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		res.WriteHeader(500)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	res.WriteHeader(201)
	_, _ = res.Write(utils.JSON(utils.NewOk(id)))
}

func (h *KeyHandler) Test(res http.ResponseWriter, req *http.Request) {
	user := utils.FromJWT(req.Context(), "iss").(string)
	id, err := uuid.Parse(chi.URLParam(req, "id"))
	if err != nil {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	body := new(struct {
		Password string `json:"passwd"`
	})
	if err := utils.FromJSON(req.Body, body); err != nil {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	if body.Password == "" {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: RequiredError.Error(),
		})))
		return
	}

	var value struct {
		PrivateKey []byte `genji:"priv"`
	}
	err = h.DB.View(func(tx *genji.Tx) error {
		res, err := tx.Query("SELECT priv FROM keys WHERE owner = ? AND id = ?", user, id.String())
		if err != nil {
			return err
		}

		ok := false

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		err = res.Iterate(func(d types.Document) error {
			ok = true
			return document.StructScan(d, &value)
		})
		if err != nil {
			return err
		}

		if !ok {
			res, err := tx.Query("SELECT 1 AS ok FROM shared_keys WHERE user = ? AND key_id = ?", user, id.String())
			if err != nil {
				return err
			}

			ok = false

			defer func(res *genji.Result) {
				_ = res.Close()
			}(res)
			err = res.Iterate(func(d types.Document) error {
				res, err := tx.Query("SELECT priv FROM keys WHERE id = ?", id.String())
				if err != nil {
					return err
				}
				defer func(res *genji.Result) {
					_ = res.Close()
				}(res)

				return res.Iterate(func(d types.Document) error {
					ok = true
					return document.StructScan(d, &value)
				})
			})
			if err != nil {
				return err
			}

			if !ok {
				return NotFound
			}
		}

		return nil
	})
	if err != nil {
		if errors.Is(err, NotFound) {
			res.WriteHeader(404)
		} else {
			res.WriteHeader(500)
		}

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	_, err = utils.DecryptPrivate(value.PrivateKey, body.Password)
	if err != nil {
		res.WriteHeader(403)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "BadCredentials",
			Description: err.Error(),
		})))
		return
	}

	res.WriteHeader(204)
}

// ShareKey to a specific user
func (h *KeyHandler) ShareKey(res http.ResponseWriter, req *http.Request) {
	user := utils.FromJWT(req.Context(), "iss").(string)
	body := new(struct {
		User string    `json:"to"`
		Key  uuid.UUID `json:"key"`
	})

	if err := utils.FromJSON(req.Body, body); err != nil {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	if body.User == "" || body.Key == uuid.Nil {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: RequiredError.Error(),
		})))
		return
	}

	if body.User == user {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: NotValid.Error(),
		})))
		return
	}

	err := h.DB.View(func(tx *genji.Tx) error {
		res, err := tx.Query("SELECT 1 AS ok FROM user WHERE email = ?", body.User)
		if err != nil {
			return err
		}

		ok := false

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		_ = res.Iterate(func(d types.Document) error {
			ok = true
			return nil
		})

		if !ok {
			return NotFound
		}

		res, err = tx.Query("SELECT 1 AS ok FROM keys WHERE owner = ? AND id = ?", user, body.Key.String())
		if err != nil {
			return err
		}

		ok = false

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		_ = res.Iterate(func(d types.Document) error {
			ok = true
			return nil
		})

		if !ok {
			return Forbidden
		}

		return nil
	})
	if err != nil {
		if errors.Is(err, Forbidden) {
			res.WriteHeader(403)
		} else if errors.Is(err, NotFound) {
			res.WriteHeader(404)
		} else {
			res.WriteHeader(500)
		}

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	err = h.DB.Update(func(tx *genji.Tx) error {
		return tx.Exec("INSERT INTO shared_keys VALUES ?", map[string]string{
			"user":   body.User,
			"key_id": body.Key.String(),
		})
	})
	if err != nil {
		res.WriteHeader(500)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	res.WriteHeader(204)
}
