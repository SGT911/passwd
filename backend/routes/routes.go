package routes

import "errors"

var (
	RequiredError error = errors.New("a field is required but is empty")
	NotValid      error = errors.New("value is not valid")
	NotFound      error = errors.New("object was not found")
	Forbidden     error = errors.New("access to object denied")
)
