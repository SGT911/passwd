package routes

import (
	"crypto/rsa"
	"errors"
	"fmt"
	"github.com/genjidb/genji"
	"github.com/genjidb/genji/document"
	"github.com/genjidb/genji/types"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth"
	"github.com/google/uuid"
	"gitlab.com/SGT911/passwd/backend/utils"
	"net/http"
)

type StoreHandler struct {
	DB     *genji.DB
	Secret string

	auth *jwtauth.JWTAuth
}

// LoadRouter create Store routing for API
func (h *StoreHandler) LoadRouter(r chi.Router) {
	h.auth = jwtauth.New("HS256", []byte(h.Secret), nil)

	r.Group(func(r chi.Router) {
		r.Use(JWTValidate(h.auth))

		r.Get("/byKey/{id}", h.ListByKey)
		r.Put("/", h.Create)
		r.Post("/{id}", h.GetContent)
		r.Get("/{id}", h.GetMetadata)
	})
}

// getKeysByUser is an internal function to simplify the key getting and decoding
func (h *StoreHandler) getKeysByUser(userName string, key uuid.UUID, password string) (*rsa.PrivateKey, *rsa.PublicKey, error) {
	var value struct {
		PrivateKey []byte `genji:"priv"`
		PublicKey  []byte `genji:"pub"`
	}
	err := h.DB.View(func(tx *genji.Tx) error {
		res, err := tx.Query("SELECT priv, pub FROM keys WHERE owner = ? AND id = ?", userName, key.String())
		if err != nil {
			return err
		}

		ok := false

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		err = res.Iterate(func(d types.Document) error {
			ok = true
			return document.StructScan(d, &value)
		})
		if err != nil {
			return err
		}

		if !ok {
			res, err := tx.Query("SELECT 1 AS ok FROM shared_keys WHERE user = ? AND key_id = ?", userName, key.String())
			if err != nil {
				return err
			}

			ok = false

			defer func(res *genji.Result) {
				_ = res.Close()
			}(res)
			err = res.Iterate(func(d types.Document) error {
				res, err := tx.Query("SELECT priv, pub FROM keys WHERE id = ?", key.String())
				if err != nil {
					return err
				}
				defer func(res *genji.Result) {
					_ = res.Close()
				}(res)

				return res.Iterate(func(d types.Document) error {
					ok = true
					return document.StructScan(d, &value)
				})
			})
			if err != nil {
				return err
			}

			if !ok {
				return NotFound
			}
		}

		return nil
	})
	if err != nil {
		return nil, nil, err
	}

	var privKey *rsa.PrivateKey
	pubKey, err := utils.LoadPublic(value.PublicKey)
	if err != nil {
		return nil, nil, err
	}

	if password != "" {
		privKey, err = utils.DecryptPrivate(value.PrivateKey, password)
		if err != nil {
			return nil, nil, err
		}
	}

	return privKey, pubKey, nil
}

// Create a new encrypted content (only text plain)
func (h *StoreHandler) Create(res http.ResponseWriter, req *http.Request) {
	user := utils.FromJWT(req.Context(), "iss").(string)

	body := new(struct {
		Name    string    `json:"name"`
		Content string    `json:"content"`
		Key     uuid.UUID `json:"key"`
	})
	if err := utils.FromJSON(req.Body, body); err != nil {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	if body.Content == "" || body.Name == "" {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: RequiredError.Error(),
		})))
		return
	}

	_, pubKey, err := h.getKeysByUser(user, body.Key, "")
	if err != nil {
		if errors.Is(err, NotFound) {
			res.WriteHeader(404)
		} else {
			res.WriteHeader(500)
		}

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "InternalError",
			Description: err.Error(),
		})))
		return
	}

	data, err := utils.RSAEncrypt(pubKey, body.Content, "text/plain", fmt.Sprintf("%s.txt", body.Name))
	if err != nil {
		res.WriteHeader(500)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "InternalError",
			Description: err.Error(),
		})))
		return
	}

	id := uuid.New()
	err = h.DB.Update(func(tx *genji.Tx) error {
		// TODO: Update if already exists
		return tx.Exec("INSERT INTO store VALUES ?", map[string]interface{}{
			"id":     id.String(),
			"key_id": body.Key.String(),
			"name":   body.Name,
			"data":   data,
		})
	})
	if err != nil {
		res.WriteHeader(500)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	res.WriteHeader(201)
	_, _ = res.Write(utils.JSON(utils.NewOk(id)))
}

func (h *StoreHandler) ListByKey(res http.ResponseWriter, req *http.Request) {
	user := utils.FromJWT(req.Context(), "iss").(string)
	id, err := uuid.Parse(chi.URLParam(req, "id"))

	if err != nil {
		res.WriteHeader(400)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	_, _, err = h.getKeysByUser(user, id, "")
	if err != nil {
		res.WriteHeader(403)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "InternalError",
			Description: err.Error(),
		})))
		return
	}

	contents := make([]map[string]string, 0)
	var value struct {
		Name  string `genji:"name"`
		Owner string `genji:"owner"`
	}
	err = h.DB.View(func(tx *genji.Tx) error {
		res, err := tx.Query("SELECT name, owner FROM keys WHERE id = ?", id.String())
		if err != nil {
			return err
		}

		ok := false

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		err = res.Iterate(func(d types.Document) error {
			ok = true
			return document.StructScan(d, &value)
		})
		if err != nil {
			return err
		}

		if !ok {
			return NotFound
		}

		res, err = tx.Query("SELECT id, name FROM store WHERE key_id = ?", id.String())
		if err != nil {
			return err
		}

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)
		err = res.Iterate(func(d types.Document) error {
			val := make(map[string]string)
			err := document.MapScan(d, val)
			if err != nil {
				return err
			}
			contents = append(contents, val)
			return nil
		})
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		if errors.Is(err, NotFound) {
			res.WriteHeader(404)
		} else {
			res.WriteHeader(500)
		}

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	_, _ = res.Write(utils.JSON(utils.NewOk(map[string]interface{}{
		"name":    value.Name,
		"owner":   value.Owner,
		"content": contents,
	})))
}

func (h *StoreHandler) GetContent(res http.ResponseWriter, req *http.Request) {
	user := utils.FromJWT(req.Context(), "iss").(string)
	id, err := uuid.Parse(chi.URLParam(req, "id"))
	if err != nil {
		res.WriteHeader(400)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	var value struct {
		Key  string `genji:"key_id"`
		Data []byte `genji:"data"`
	}

	var body struct {
		Password string `json:"passwd"`
	}
	if err := utils.FromJSON(req.Body, &body); err != nil {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	if body.Password == "" {
		res.WriteHeader(400)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "ValidationError",
			Description: RequiredError.Error(),
		})))
		return
	}

	var keyId uuid.UUID
	err = h.DB.View(func(tx *genji.Tx) error {
		res, err := tx.Query("SELECT key_id, data FROM store WHERE id = ?", id.String())
		if err != nil {
			return err
		}

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)

		return res.Iterate(func(d types.Document) error {
			err := document.StructScan(d, &value)
			if err != nil {
				return err
			}

			keyId, err = uuid.Parse(value.Key)
			return err
		})
	})
	if err != nil {
		res.WriteHeader(500)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	priv, _, err := h.getKeysByUser(user, keyId, body.Password)
	if err != nil {
		res.WriteHeader(403)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "BadCredentials",
			Description: err.Error(),
		})))
		return
	}

	data, options, err := utils.RSADecrypt(priv, value.Data)
	if err != nil {
		res.WriteHeader(500)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "InternalError",
			Description: err.Error(),
		})))
		return
	}

	res.Header().Set("Content-Type", options["ContentType"])
	res.Header().Set("Content-Disposition", fmt.Sprintf("attachment: filename=\"%s\"", options["FileName"]))
	_, _ = res.Write(data)
}

func (h *StoreHandler) GetMetadata(res http.ResponseWriter, req *http.Request) {
	user := utils.FromJWT(req.Context(), "iss").(string)
	id, err := uuid.Parse(chi.URLParam(req, "id"))
	if err != nil {
		res.WriteHeader(400)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "SerializationError",
			Description: err.Error(),
		})))
		return
	}

	var value struct {
		Key  string `genji:"key_id"`
		Data []byte `genji:"data"`
	}

	var keyId uuid.UUID
	err = h.DB.View(func(tx *genji.Tx) error {
		res, err := tx.Query("SELECT key_id, data FROM store WHERE id = ?", id.String())
		if err != nil {
			return err
		}

		defer func(res *genji.Result) {
			_ = res.Close()
		}(res)

		return res.Iterate(func(d types.Document) error {
			err := document.StructScan(d, &value)
			if err != nil {
				return err
			}

			keyId, err = uuid.Parse(value.Key)
			return err
		})
	})
	if err != nil {
		res.WriteHeader(500)
		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "DatabaseError",
			Description: err.Error(),
		})))
		return
	}

	_, _, err = h.getKeysByUser(user, keyId, "")
	if err != nil {
		res.WriteHeader(403)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "BadCredentials",
			Description: err.Error(),
		})))
		return
	}

	headers, err := utils.GetStoreHeaders(value.Data)
	if err != nil {
		res.WriteHeader(500)

		_, _ = res.Write(utils.JSON(utils.NewError(&utils.ApiError{
			Name:        "InternalError",
			Description: err.Error(),
		})))
		return
	}

	headers["KeyID"] = keyId.String()

	_, _ = res.Write(utils.JSON(utils.NewOk(headers)))
}
