// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    ssr: false,
    appConfig: {
        apiURL: process.env.ENDPOINT ?? 'http://localhost:9090',
    },
    pwa: {
        icon: false,
        meta: {
            lang: 'en',
            author: 'Everest-Code & SGT911',
            name: 'Passwd',
            description: 'Simple Password & Secrets manager',
            theme_color: '#1C75B0',
        },
        manifest: {
            lang: 'en',
            name: 'Passwd - A safe place',
            short_name: 'Passwd',
            description: 'Simple Password & Secrets manager',
            start_url: '/?pwa=true',
            display: 'fullscreen',
            background_color: '#fff',
            theme_color: '#1C75B0',
            orientation: 'portrait',
            display_override: 'minimal-ui',
        },
        workbox: {
            enabled: true,
            autoRegister: true,
        },
    },
    experimental: {
        payloadExtraction: false,
    },
    pinia: {
        disableVuex: true,
    },
    css: [
        '@fortawesome/fontawesome-svg-core/styles.css',
        '~/assets/styles/reset.css',
        '~/assets/styles/fonts.css',
        '~/assets/styles/global.css',
    ],
    postcss: {
        plugins: {
            tailwindcss: {},
            autoprefixer: {},
        },
    },
    modules: [
        [
            '@pinia/nuxt',
            { autoImports: ['defineStore'] },
        ],
        '@kevinmarrec/nuxt-pwa',
    ],
})
