const colors = require('tailwindcss/colors')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue",
  ],
  darkMode: 'media',
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.neutral,
      principal: '#1C75B0',

      danger: colors.red,
      warning: colors.amber,
      success: colors.emerald,
    },
    fontFamily: {
      sans: ['Nunito Sans', 'sans-serif'],
      mono: ['JetBrains Mono', 'monospace']
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
