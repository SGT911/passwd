export default defineNuxtPlugin((app) => {
    const store = useUserStore();
    const router = useRouter();

    router.beforeEach(async (route) => {
        if (!store.isLoggedIn && !route.fullPath.startsWith('/login')) {
            console.log('The user is not logged in');
            await router.replace('/login');
        }
    });
});