import {v1 as uuid} from 'uuid';

export const useUserStore = defineStore('user', () => {
    const fromStorage = storage.restore<{email: string, token: string}>('user');

    const token = ref<string | null>(null);
    const email = ref<string | null>(null);
    token.value = (fromStorage != null)? fromStorage.token : null;
    email.value = (fromStorage != null)? fromStorage.email : null;

    watch([email], () => {
        if (email.value === '') {
            email.value = null;
        }
    });

    watch([token], () => {
        storage.sync('user', {
            token: token.value,
            email: email.value,
        });
    });

    const isLoggedIn = computed(() => token.value !== null);
    const tokenBody = computed(() => {
        if (token.value == null) {
            return null;
        }

        let body = token.value.split('.')[1];
        return JSON.parse(b64.decode(body)) as {iss: string, name: string};
    });

    const clear = () => {
        email.value = "";
        token.value = null;
        storage.sync('user', null);
    };

    return {
        // States
        email,
        token,

        // Getters
        isLoggedIn,
        tokenBody,

        // Actions
        clear,
    };
});

export const useAlertStore = defineStore('alerts', () => {
    const alerts = ref<Alert[]>([]);
    const alertDelays: {[k: string]: any} = {};

    const push = (alert: Alert): string => {
        let id = uuid();
        alert.id = id;
        alerts.value = [...alerts.value, alert];

        alertDelays[id] = setTimeout(() => remove(id), 2000);

        return id;
    };

    const remove = (id: string) => {
        alerts.value = alerts.value.filter(el => el.id != id);
        if (alertDelays.hasOwnProperty(id)) {
            clearTimeout(alertDelays[id]);
            delete alertDelays[id];
        }
    };

    return {
        // Actions
        push,
        remove,

        // Getters
        alerts: computed(() => alerts.value),
    };
});