export const endpoint = computed(() => {
    const conf = useAppConfig();
    return conf.apiURL;
});

export const b64 = {
    encode: (txt: string): string => window.btoa(txt),
    decode: (txt: string): string => window.atob(txt),
}

export const storage = {
    sync: <T>(key: string, value: T|null) => {
        if (value == null) {
            return window.sessionStorage.removeItem(key);
        }

        window.sessionStorage.setItem(key, b64.encode(JSON.stringify(value)));
    },
    restore: <T>(key: string): T|null => {
        let item = window.sessionStorage.getItem(key);
        if (item == null) {
            return null;
        }

        return JSON.parse(b64.decode(item));
    },
};

export const getResource = <T>(url: string) => {
    const store = useUserStore();
    const router = useRouter();
    return async (): Promise<T> => {
        let res = await fetch(`${endpoint.value}${url}`, {
            mode: 'cors',
            method: 'GET',
            headers: new Headers({
                'JWT-Token': (store.token as string),
            }),
        }).then(out => out.json()) as ApiResponse<T>;

        if (!res.ok) {
            if (['BadCredentials', 'Forbidden'].indexOf(res.err.name) !== -1) {
                store.clear();
                await router.replace('/login');
            }
            throw new Error(`${res.err.name}: ${res.err.desc || 'No description'}`);
        }

        return res.body;
    }
}