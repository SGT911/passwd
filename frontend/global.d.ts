interface ApiResponse<T> {
    ok: boolean;
    body: T;
    err: {
        name: string;
        desc?: string;
    };
}

interface KeyEntry {
    id: string;
    name: string;
    owner: string;
}

interface Alert {
    id?: string;
    title: string;
    description?: string;
    color: string;
}

interface StoreKey {
    owner: string;
    name: string;
    content: {
        id: string;
        name: string;
    }[];
}

interface StoreHeaders {
    ContentType: string;
    CreatedAt: string;
    FileName: string;
    KeyID: string;
    Length: string | number;
}